from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.views import defaults as default_views

from config.utils.helpers.error_page_handlers import handler400, handler403, handler404, handler500

handler400 = handler400
handler403 = handler403
handler404 = handler404
handler500 = handler500

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('allauth.urls')),

    # custom page
    path('', TemplateView.as_view(template_name='pages/index.html'), name='index'),
    path('profile/', TemplateView.as_view(template_name='pages/profile.html'), name='profile'),
] 

# to handle static files
urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            TemplateView.as_view(template_name='pages/errors/400.html'),
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            TemplateView.as_view(template_name='pages/errors/403.html'),
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            TemplateView.as_view(template_name='pages/errors/404.html'),
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", TemplateView.as_view(template_name='pages/errors/500.html'),),
    ]
