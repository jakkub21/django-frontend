# Django Starter

> This is boilerplate for project with Django Template Architecture.\n
> This boilerplate is suitable for projects that want to speed up their Django setup process.

## Step 1: Prepare your machine (prerequisites):

1. **Install Python 3.7**: (Python Official Website)[https://www.python.org/]
2. **Install PIP**: (PIP Official Website)[https://pip.pypa.io/en/stable/installing/] - this step is optional, pip should be included with Python 3.7 installation

## Step 2: Prepare your Environment (Virtual Environment):

**PIPENV Installation:**

```
pip install pipenv
```

**Or if you don't have permissions:**

```
python -m pip install --user pipenv
```

**Clone project:**

```
git clone git@gitlab.com:jakkub21/django-frontend.git
cp .env.example .env

```

**Create & Activate your Virtual Environment & Install dependencies:**

> sufix `--python 3.7` will specify which Python version to use if you have multiple python versions installed

**Create `.venv/` folder in your root directory of this project and then run:**

```
pipenv shell --python 3.7
pipenv install --dev
```

**To see your installed dependencies use this command in active virtualenv:**

```
pipenv run pip freeze
```

**Migrate database & start development server**:

```
python manage.py migrate
python manage.py runserver
```
